import { createApp } from 'vue'

import { store } from './storage'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import hosts from './gateway-config'
import $ from 'jquery'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.bundle.min.js'
import '../public/css/simple-sidebar.css'


const axiosInstance = axios.create({
    baseURL: hosts.api,
    timeout: 1000    
  });

createApp(App).use(VueAxios, axiosInstance)
              .use(store)
              .use(router)
              .use($)                                              
              .mount('#app')

