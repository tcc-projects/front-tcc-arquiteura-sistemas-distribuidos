import { createStore } from 'vuex'
import mutationsDefinition from './mutationsDefinition'
import loginService from '../services/LoginService'
import mapaService from '../services/MapaService'



let myStore = {
    state: {
        points: [],
        fixedPoints: [],
        fixedCircles: [],    
        circles: [],
        labels:[],
        currentView: 'Home',
        accessToken: null,
        projects: [],
        properties: []
    },

    mutations: mutationsDefinition,
    getters: {
        allPoints (state) {
          return state.points
        },
        allProperties (state) {
          return state.properties
        },        
        allCircles (state) {
          return state.circles
        },
        allProjects (state) {
          return state.projects
        },        
        currentView(state){
          return state.currentView
        },
        allLabels(state) {    
          return state.labels
        },
        accessToken(state){
          return state.accessToken
        }
      },
    
    actions: {         
      login(context, credentials) {
        return new Promise((resolve, reject) => { 
                      loginService.login(credentials)
                                  .then(token =>{
                                                  localStorage.setItem('access_token', token)
                                                  context.commit('setAccessToken', token)   
                                                  resolve(token)  
                                                })
                                                .catch(error => {           
                                                    reject(error)
                                                })
                  })        
      },

      logout(context){
        context.commit('setAccessToken', null)
        localStorage.setItem('access_token', null)
      },

      refreshMapReferences(context){

        mapaService.getReferences()
                  .then((response) => {                            
                    context.commit('refreshLabels',[])
                    context.commit('refreshCircles', response.circles)
                    context.commit('refreshPoints', response.points)
                  })
      
      },

      findPropertiesByDocument(context, document){        
        return new Promise((resolve, reject) => { 
                    mapaService.getProperties(document)
                              .then((response)=>{                                
                                context.commit('setProperties', response)   
                                resolve(response)                   
                              })
                              .catch(error => {                                   
                                context.commit('clearProperties')                                          
                                reject(error)
                              })})
      }

    }
  }

export const store = createStore(myStore);