const mutationsDefinition = {
      setCurrentView(state, viewName){
        state.currentView = viewName;
      },

      setProjects(state, projects){
        state.projects = projects;
      },

      setProperties(state, properties){
        state.properties = properties;
      },

      clearProperties(state){
        state.properties =[];
      },

      setAccessToken(state, token){
        state.accessToken = token
      },
  
      refreshLabels(state, labels){
        state.labels = labels;
      },
  
      refreshPoints(state, points){                                     
        state.fixedPoints = points
        if(state.points.length >0){              
          let types = [...new Set(state.points.map(el => el.label))];         
          state.points = points.filter(el=> types.find(t=> t===el.label)=== el.label)    
        }
      },
  
      refreshCircles(state, circles){                                   
        state.fixedCircles = circles 
        if(state.circles.length >0){              
          let types = [...new Set(state.circles.map(el => el.type))];         
          state.circles = circles.filter(el=> types.find(t=> t===el.type)=== el.type)    
        }
      },
  
      removeReference(state, label){
        state.points = state.points.filter(p=> p.label !== label)     
        state.circles = state.circles.filter(p=> p.type !== label)
      },
  
      includeReference(state, label){
        if(state.points.findIndex(p=> p.label===label)<0){
            state.points = state.points.concat(
                                state.fixedPoints.filter(p=> p.label === label))
        }
        
        if(state.circles.findIndex(p=> p.type===label)<0){
          state.circles = state.circles.concat(
                              state.fixedCircles.filter(p=> p.type === label))
        }       
      }
      
    }

export default mutationsDefinition