import http from './HttpService'


const getProperties = (document) =>{
  return new Promise((resolve, reject) => {
    http.get('/impostos?documento='+document.value+'&tipoDocumento='+document.type)
    .then((response) => {     
      resolve(createProperties(response.data))})
    .catch(error => {           
        reject(error)
    })
  })  
}

const getReferences = ()=> {
    return new Promise((resolve, reject) => {
        http.get('/mapa/referencias')
        .then((response) => {                            
                        resolve({  points: createPoints(response.data.pontosGeograficos),
                                   circles: createCircles(response.data.ocorrencias)})
        })
        .catch(error => {           
            reject(error)
        })
    })   
}

const createProperties= (propertiesResponse)=>{
  return propertiesResponse.map(p => {
                                      return {
                                          position: p.localizacao,
                                          icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABbmlDQ1BpY2MAACiRdZG9S0JRGMZ/alGUJZRDRIODRYOBFERj2eAiIWaQ1aLXr0Dtcq8S0hq0NAgNUUtfQ/9BrUFrQRAUQURL/0BfS8jtPSkooedy7vvjOed5Oec5YA/ltLzZ4Yd8oWhEggHPcmzF0/WGAzf9DOCIa6Y+Fw6HaDu+H7Cpej+herXf13L0JlOmBrZu4WlNN4rCs8KhzaKueFfYrWXjSeFjYZ8hBxS+UXqixq+KMzX+VGxEI/NgVz09mSZONLGWNfLC48LefK6k1c+jbuJMFZYWpQ7LHMEkQpAAHhKUWCdHkQmpBcmstc//51tgQzya/HXKGOLIkBWvT9SSdE1JTYueki9HWeX+P08zPTVZ6+4MQOeLZX2MQtceVCuW9XNiWdVTcDzDVaHh35CcZr5ErzQ07xG4tuHiuqEl9uFyB4ae9LgR/5McMu3pNLyfQ18MBu+gZ7WWVX2ds0eIbskT3cLBIYzJftfaL4jSZ9NI0AgrAAAACXBIWXMAAAsSAAALEgHS3X78AAAWEElEQVRoBXWaCbBV9X3H/2e599x738KDx/LggYDIEmyjQUCiRDGJRILEuICAEydq1YLWRBJHTTt0EtPGWq3M2KjjVJERrYCiIqTGJdUEiXQ6aEqRgDzAjX15693vOf1+/ueeFzT2P3Pu+d//8tu3//9eZ9euXaa7u9t0dHS4Rq1QKIQDBgxwNRaVy+Vo6NCh3uHDh8NUKmVaWlrczs7OWhAErud5ZuHChSF7aP897zujjOOcZ8LwXBOZLxnHjIzCcLjGWuyCKOo0rnvQMeYTPTsj192q8S1TN7z0sZ3Xx6pVq9xMJuP09vbWoKGvr8+USqVw8ODB3tGjR0Pf983AgQNd9Wu5XM5Np9NGc6FoNM7GjRvNtm3bMtogusswlTnrrLPyO3bsSFWrVe/RRx8tLlmyJCPCw+nTp5fffvvtXNDcXHzo/vvDW268MXf9gYNfDx3nBnE0Kyeia45joigyjp6SiONN0xqToaM3j6fxAswZ86YfRY+vHDbsjYeeeLzww5/c7eSPHc9Nnjy5T8L1RVfqjjvuKDzwwAMZx3HCRYsWlVevXp0Tw6VBgwaFlUolM2XKlAKMuJs3bzZ79uzxLRrHqeqd0lPTAxXCE1UFRBQY7610qnzk6WfCN+de+s1GY/5eRM30NFUNQ1MVcaiVTTz0E5UlfYDwMM6+tOvafeJ+c96Yn16waePrzdcsdr9VrqS1pCbckVBDT8UKyHGgp6IxySJy2traqhdeeGHkQl8oIvSmaT0Cs28+AEKL+LLL98NfdPektsy99BcNjvMfvuvOFHRTqtVMWJc80rYPgNS09zPfk7mEWfYCA1hZwRTse1d096R3pvya3QnuU+ii/7nvdt7du3dvKJPKyP7gGE6zwl+sbwjUL6gfvJbJhI90dacnhNEzja57lyTo90kAEo8lFJNCpUjaFQWBvjOGaWFG9BnzNIeZIWLWigoLA1jC7wv2neD4ZWd3sLaxAUvAIqEhpQcU0JbVWsk/LMv8sx9//HHkT5w4EcfOf/rpp9ig1pk+PTk9FT1FPY29jpNf2dnpC+nqFte9rFdIhdWge3FuXPp6V8VBpVIzh+Rrx8sVU9BT1hoaa7PplBmkZ4ic1El5YkDMaC8wUnrDGAy1eN7lnbXaU6uPnVi4KpfND4yiBk0V9GCh9PtEq1A6KflK37hx41xfkSE6ceIE3GKPWmOFBRNw5Yvg8q9mzgh/8sob9zb5/mV91Wq/uWiDlbYrBrrLVdPR22cOFUqmKHOhWbHYXvwRlcpmn7qKHKYtG5hxjQ2mKS0hhzETmB17eoWjyfO/O7ZWvfe1EW3LFnxyoKI5tEFDNsgFYquyplRPT0/NhlmpSH7nSDgRQmEDlDha6a1dt678+q9/892M6/6gjC8wYScjaxaIqKOnz/z26HHzod5lSRQnBohgCpViR515xpgraQ1rf6c97AUGJoYZAhsc5bBmAuF8fXfHZdAgWljGNLR5ekRuhPB9MRI5r776qjt79uxw/vz5+AbIUSHqKwfVWmWciQbNKZZ+o+hyVl6I0npkuBaiK0m+29VtPurNx5AVgRwrC/mHmPZdz7ScOVmii0zX/+6wFLh2jTTAWwxB9ChpZkpLk/UjvmNmmJtsnWj47suZ4Bs7Pa9TkQlmMPseMYD/euvWrctv2rTJ8y655BIzevTojJKQ9oTyybqzR5H3zMoHa/c++9KVac9bgqSRKLEZh3UlIsuEJOrJtJAovoK+FdzNgNGjzbhrFpnRl841w6ZPN8Gwoaa3Y68pK8kZmRZrMSWezlLJIKQR2YzELkEJhsWlMd/zho+rVP5n2Usvbj9z8mQ5l4PfInSWEXqDpqamqk92lJQwK/EA5Fhwx103uuWGZdjiX+mxDSIRiWRp9sgfPpQmFDbtnN0oBlJNTaZ97rdN+0UXGS+bNc25jImkuWjaNNMs5vasWWOOb99uHE+kCh37PMEAVrMy9/imBssMuGjMq9102/z5z+12nbA5TlWYF4ilWDdUpeG4SvmONFIWI9gdDwk5/XouW1mcz49XqJxRkzbgkRCqqGBOiuA/dvdaqaGJSGYUyQ9azznHnP3jH5nRc+eaIJc1o9uGmhFDh+gZbIYPajGZwYPN5JtvNmdccaUwpO0e4ZI2Yw0A8wTCqOOCUVkJ4XrGVfnC+FfWrCUIYVI4PDymGxoayuLBuMrooXJJTiWIzSOaVMI2ebPyScVf51tNrhuQsJCQtV29O4SQTE4+iIpFk2ltNROvv8785c03mWzbMDOwMWfGjznNBCnfCsCTpBvF2OkjR5hsJmOGz77YnH3rrSbbPsLUFKrdmnKItFLVe59gQ6XNT3rTV24JhP9idWk2PUjwOHpBjp4TD5E7cuRIii8l2BqRC40UFQ9sWZSJonNin4gVDOGFStUcLpatOGS/pv3ii81X7rrTjJg50wS+Z8a2DzfDBrdKkjUDA0jUOr76aeWO0e1tplXm03jGOPOVZcvMiK99TXWINC6tEsYPCXZRONA0zSZTvRVkptqBmLaSaKUqSSuRl9rb2+0XfASBs5MnKgvIsiuvTOvLRHJF0pg8WiyZioiLqhUz7KszzKTrvm9q0sr+554zwwY0mZz8QpLSk+yycdJqJpSvELWGDxtihjY3GpXQZpI0OfHaa42fUzDSJoLKEeGo82GBQIOonvjjK65IE83U+qELV0RVbEuUYrFIiaKaL8IGsy+uXVs8u1JpUpgdQXSJ01vsQEdlwzRPofXE7t1mz+rV5t1/vt8cfOcdmX3aVOQrMELZr8rUEu5LU+VyyY4xV5XEByhCvf/YY+boe++Z0bNnm8EKBpHWW2Hp7dVJxS/rNLRPqtYGvCDahF7GohCiZC1tZ/bv3x/5kyZN4ozxmRKl/ZrF6aFdPc0pEzVINrFTa3dRRBSVwa1MRFzp4CGz75NPkZZpUnhVyW0aJNlQZgLhiiaWGRgPgozG5A/SCEz29Paa4qHD5v1HHjUHN79t+vbuVdkijIJVUmlTEi7g4iuyI6JQbnStlhu7eHGgBJvHrAQ2lc1m87ZEUf6IxEhSokBjKu+6VWXZjJJTGqchScUKleoFOO5rUKaBn9ioBbK6TyB1VxpTQWfNRyvjvpig1eQ/7HMUDMJqzRzfts042iviNAuOU8xRI3Ua0qFjsnm3/5gBWSHnlf4Spe7oqAorSp1UuS5YVAwWNVrmsWgsF/qihvRYZFmDEa1WTcqMeBR6ish83lQLKhbU97yYkdhXIE/AGBcTMSa71YbjeKUoFVz6wu/KFrzDQQCN5EuRa83LRxmuPN5dsWJFUdGFahJ19Z1eKgcST0moinhEHaXNI7ZaE3AYc0W0LTVgRX1MC8ejkiA8H/7DdrNuya32ObZjJ6ck68SYVkmmxx4EAQxg0RBOWsDxDXAkIR9a5Bh94wsFIqrOYJRtTvDwww8XbNQ6cuRIdPvtt6s+c0mK0J1T7C5/4ron5ezdEI4IAErtEyA9CNJ3K61+Alw5u8y3Fjs7jv+HFzeYro8+Np37PzLbnn/BpDSvrdZvAhJi3dSsQATbNi1Iy+Q4u9AI/3Uaujt8r1vsk7ApUbCg0tKlSzOHDh2KXKQjzlATcdnSd36l4t/X1NgprB04XNLoDdR5AqZofE/iPazGRhavtxmZqCXGXXxBjh7JN2jAFD7b54PMHu8CirFnluQ785aGyOx5qCHX9dVKBQOxm0UuVkeJYmyJMm3atFNLlKJWpXc//XSoHVtQO0kJwEinLZOOfUF96x91jPSrEI4Ti1Dr6BCsdTyU71U5tl7WJwjNcaUMI4mvyfiFDxzgouEM1nydaMv2Z/69prWYFiEYJnwldEoUx/3ggw/CrVu3ZuslChVBg7D1mb++yahu3qKMGxFwLTEiNieTGIxW1I9tGz2IGEWpjEIvdRnUZlSSWAI0x8GmJgIDJUuD/8j8slrLHvZyHGAtMIENDoRHIwFCQ49xfi+aEFuvngatpUQp6pgblyg4iuyZxILzIABsMDi9p9c76bhblDs+TCuy0BJ7PV0lBlLHWa001a8oLxzZs8eUe3pMsavLHN//ockfOSK6VSsrWtFnrNjZpTW95vCeDlPp67VwEo0IjBkr2IlfgpMjNDSoGv/9wN48WkAj/SWKFFCSVhS9fV+4+ksU9lohTSlX3IUvbzjxu7mXblCSuY0oRJOHmSGZwJzWkDX7Od3BkIglzP7qb5ebdJZzj8xMSQ3m8BHaSTGx/tYfGE8SRyzlQtHUFJbZC0J8aoyYADY4GMNcuS5S9tsALTr8WYfWVKwu9CvaCfWe/CM6duxYVjeMaEWByIGSvJD5h6ZPc67ryx+UvV2ncTKDVTXvIXKwYyo1Cti6mLERSGfykuquioiMFIpVBP3JiUVQyJyesh5HjEaqDmCK3NOqkuUclfr4kr2M0LgAk4DLujJZ8uLM84+NLVeI7fgHpmWrdWV26K24utFzde2Yl3P25xFN5LQhaimWaksHDtghybyFZEDAUdcWcTK3GYMGmIEigIs5e2wlQmkdD0wkuUHwbN8STgIU+br/jH1HTADjXMFCu8ACh8UlOPKV/1wwpHXn2HyBJIR1J7coyr9uhhJl/PjxjpvP57lF8SVVbMdmdr1t0Jhaq6X+a/XTZV0k/SuelRSQVgNClpKOZrS2mLE6f5BT4qyu3WpxREPecYv9gL7W1deCEHMCBrCEQrTqAKdxi0sDutB6uOPW6ypjdEHCsB5yXXItpsK76IsH4548eVLLo5S4g1Zg2+tJvbni8xbNn+9sCtKvqbx+RzcpFgnkgZADF8541sABZqqIaZX9AwCpwhR9weQj7muMOcZZO017vqy9wEBywAQ2jIBLx4V3XgqCNxbf+0tLC0v0wAiOB91s81UrRu6oUaPcBx98sCD/oHAkYCQXdOArKGw23Ld+fUElw8/lKxELmOChz9mF/iiZx/Shreb8wQPNBEm5Rd91srOSpkCkzxhn8plaw9p2facBI7mgg4n6WTZSaPqHf1n/fF/Vc+2tjqYS00pKlAwlinhwnGeffdZRHkkfOHBAvHBKc9EI+SRRpdzSpL5cqYbfLpU2iqjZVa2T+Kyv2ISlPpJnA2ZDtObCgbIfbdimcUocHeW4j7OmB9FoATOCQhsB1cfMFHleeTUIvvNeyueaAnqgi4ZGgro2wiFDhqSmTp1aEt2K83GJwhvVgZk3jb6LtH56zx0VSehnsj9ieP+iOpn9i/leE6W8CRCBiOJmkX4y1x9eLbrPIgS2zK8EruVzZlW4eocGPbyTPkaA8CCZE2J8i7JgwQJKFISLUAlvaJfFSCIjDsuX3LPCv3JI61YF/CctUYLPBi7raGykD0aIoXolOwMEydNnTJOfXVvfByxopkRRBFx1YXvb1rmvbyZvxDTEZpVYHklbCnRSI0aM4HcSx923b1+4Zs0aSpSk+sUesUFoosqkJMg0K2ocfXJVdZfv3SfHP4SDJratecuEvYVUH7HZ8ltrAAKT/99aGGWfndd6Jd4DO33/PvPYv4WNGldLaEC48IsPQyPBqajL95x+EArd4cOHK9N5JEOcHZz2XktvoCANAJD1wgXz5wff27Bhr1T2M0ySY6jVsRYgcewcADTb1xhAWMNaxElL1lrt6TtrmedgJtj3XLvhpY75CxYE+JtaQgPLAAU9iUbIffEtighyVAZb1rWAnSwGB41xaIvnVTYrHLsbguBxhblfY/unbvx8Pza6eDP9ZF5d2z91HljAXJ8JVgoHt7JMJ9ugB7oAkfTtvJRgSxRrWvppof+HHi0k5XORzcJg7dq1mBlSAEBBfpC7f/3zZUnr9nwYdmNiRCb8g7CC1MFok5HGAIIkyNTYBQ1fSdbiF0QzwerSmh+ueP75Eji0DF+FcGI09BDtAUE/p32YVknHgawuGCP3jDPOcJubmwt/VqLEQIoKBNgjQKExJ8L6rpLaz9+0cafC5t9R9IkOaxoJsXBM/9TckPiMpuK1esMga6nTgHXhpo1/lEllBA6/hBnkAOF/lkdwAz0ZVe75CRMmxCWKNMIggkyEiT0CBAkQtxOc1pyFNDpv4UJ/byZ4RFeUr+rH6tgeJd1+/df7AOFhnHxBs3290YZu+m3OeD+XfeSCq6/2VdqcSgPLrULrbyujeh9gNSmAywfDnZaqiVBRz5YoSN1ahd6nMpJYBUxxtV/THVN0fXMT0WGJfrg5iPNb6WoBjT71V8II5sQYjXGow7kVAQ8K4S1LGxvcESphBBthJTRAbNJPhBnTIDAShFJO5FNmuaeddholin4ts1GLTUl4QzKolUttTIvvqJv5lAhLXX2yszL9pu/tU1i5QxtD/ARsEP9500oqWk31h2ZJJ1S99qOp37xg3xVd3RUxyHb8ERyEXRo+Cg1EKpjAzJjHiuwtinhwvDlz5hh1AqnHFo1MapENb3rDWBKO1bWAcD4bCiueFwzcsav2T81N2xflC6N0cz+Fn9VwGsIpiyg70IS1Sb2ZQyKNMqm+KHx85umn/eP5u/e6+rmbxItFsBQchF1kAj0IEmcHFOMJDbVZs2al9Y+Mmv0rhoiXhqz9spEO76QlY3w/tQ9R0cgwdMfrNLnX8+5U+HyPkoQo9kVALEDNsUYm8O5+z7vrL050eaMEA1h1hAkO3jzwnYyxJOkn89oqYZHe9feILypRAIw2EukAMJEOPsNTlDkEX6pWXSWx43KUJXK4Lh1ubDhmAUDYSN+anua0plNrl16zYcPxMysVBxiaRupIHCugzxgt0QBmlWgrWZvWwbCkg6HjzZw5M1q9ejVH3Yr8RDAdbNCWyXoDDHvEN2gASOzVhmN9z0sk3uQzzwyWv/DCvoUTJhzR5do8mZZD2XGqz+AnCtdhlzF/M2fTxpcVarNWnH8KsQmxCQ0whiChAVr4noRje9RVxM3qbq7i6p81ugRP8Q8CnEfVQ6SL8Eg4bUNTlMy8aYwzLzrtk6zFnCrkl6+3DV2pSPRQSkdZ/ASN8NDXrzL8NP3QN9qGPsFa7SHqEH3AASwiu1JOPw3YPOPMWx+u960Pi16dKrwyPLg68zqNjY1ogmqSaxUFpPgHIC1iXC9POculZtZ9s51HkLog8ZN9lNIydOPNKpacE45zd1+t9ltbwsCMHvoae3O/6959QanMFSFwdZ1l4SawBFbhMEVQjGmo4/iitZYG/fOBvzwZT1cska7lA32pKMOHYiyjvzPpcqTi6YfG1OWXX17Q/6P0z6bmSFUAksjKJrndqwlIRgf/gpB52u/r/1uF3gMHgttWrSouWf30mxXXvUpH1mbKftnkJ34Yzrtk48vHr9i8OaOKoaD/gaX1vyznvPPOK+ncnVX0qagsr4qR7JgxYwr8WgsNy5cvL+hvV2nR4MybN6+kW5+M8Fd1qKpqPz9PVxzVKUa3jfwIo0uP0GhRxKUdiZJDPZdfOj1G3K8SGPgDC86lZsRgJMSOBGG4xGAfa+Vs5sYbboi2XjrvItViT4h5Et/3z9208a1VTz3l1HSFxPH04MGDtuADHvta9aOqmgNcYAluJB8w9LmollqM1th9Ypqbfx1dwlAOb/4Pl5njAlUh4BoAAAAASUVORK5CYII=',
                                          info: p
                                      }})   
}

const createCircles= (spots) =>{      
        let types = [...new Set(spots.map(spot=> {return spot.etiqueta}))]
        let circles = []
        
        types = types.map(t=> {
            return {
              type: t,
              color: Math.floor(Math.random()*16777215).toString(16)
            }
        })                     
        
        for (const key in spots) {
          let color = types.filter(t => t.type===spots[key].etiqueta)
                             .map(t=> {return "#"+t.color})[0];                                                                              
          circles.push(
          {
            center: {lat: spots[key].lat,lng:spots[key].lng} ,
            radius: Math.sqrt(spots[key].numeroOcorrencias) *10,
            strokeColor: color,
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: color,
            fillOpacity: 0.6,
            type: spots[key].etiqueta,
            occurrences: spots[key].numeroOcorrencias              
          })            
        }

       return circles;     
  }

const createPoints = (apiPoints)=> {
    return apiPoints.map(p => {
                            return {
                                position:{
                                    lat: p.lat,
                                    lng: p.lng
                                },
                                label: p.etiqueta,
                                title: p.titulo,
                                description: p.descricao,                            
                            }})   
  } 
  
const mapaService = {getReferences, getProperties}
export default mapaService