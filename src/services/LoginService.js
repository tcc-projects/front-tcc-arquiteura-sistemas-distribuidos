import http from './HttpService'

const login = (credentials) => 
              {
                const params = new URLSearchParams()
                      params.append('username', credentials.username)
                      params.append('password', credentials.password)        

                return new Promise((resolve, reject) => {
                        http.post('/login', params)
                          .then(response => {                                           
                            resolve(response.data.access_token)           
                          })
                          .catch(error => {           
                            reject(error)
                          })
                    })
              }

const loginService = {login}

export default loginService