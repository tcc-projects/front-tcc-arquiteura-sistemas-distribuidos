import axios from 'axios'
import hosts from '../gateway-config'
const http = axios.create({
    baseURL: hosts.api,
    timeout: 10000    
});

export default http
