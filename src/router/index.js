import { createWebHistory, createRouter } from "vue-router";
import MapaCidade from '.././components/MapaCidade'
import Home from '.././components/Home'
import ListaProjetos from '.././components/ListaProjetos'
import Login from '.././components/Login'
import CadastroProjetos from '.././components/CadastroProjetos'
import axios from 'axios'
import hosts from '../gateway-config'

const cadastroProjetoPath = '/projetos/cadastro';

const routes = [
    {
      path: "/mapa",
      name: "Mapa",
      component: MapaCidade,
    },
    {
        path: "/home",
        name: "Home",
        component: Home,
    },    
    {
        path: "/projetos/lista",
        name: "ListaProjetos",
        component: ListaProjetos,
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
    },
    {
        path: cadastroProjetoPath,
        name: "CadastroProjetos",
        component: CadastroProjetos,
    },
    {
      path: '/projetos/:selectId',
      name: "DetalheProjeto",
      props: true,
      component: CadastroProjetos,
    }
    ,
    {
      path: "/:pathMatch(.*)*",
      redirect: '/home',
    },
  ];

  const router = createRouter({
    history: createWebHistory(),
    routes,
  });


  let verifyLogin = next => {     
    let token = localStorage.getItem('access_token');       
    if(token == 'null'){
      next('login')
    }    

    const params = new URLSearchParams()    
    params.append('token', token)

    axios.post(hosts.api+'/login/validate', params)
         .then(response => {           
            if(!response.data.active)
              next('/login')
            else
              next()
           })
         .catch(() => {           
           next('/login')
         })    
  } 

  router.beforeEach((to, from, next) => {            
    if(to.href.startsWith("/projetos") && !to.href.endsWith("lista")){
      verifyLogin(next);                       
    }else{
      next();    
    }
  })

  export default router;
  